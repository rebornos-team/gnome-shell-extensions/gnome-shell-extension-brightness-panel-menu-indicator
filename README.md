# gnome-shell-extension-brightness-panel-menu-indicator

Shows a brightness indicator on panel menu if a backlight device is avaliable, allowing to change brightness through scrolling.

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-brightness-panel-menu-indicator.git
```

